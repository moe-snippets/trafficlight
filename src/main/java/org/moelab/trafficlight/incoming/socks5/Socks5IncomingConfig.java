package org.moelab.trafficlight.incoming.socks5;

import org.moelab.trafficlight.incoming.IncomingConfig;
import org.moelab.trafficlight.incoming.socks5.authenticator.Socks5PasswordAuthenticator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author xp
 */
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class Socks5IncomingConfig extends IncomingConfig {

    private Socks5PasswordAuthenticator authenticator;
}
