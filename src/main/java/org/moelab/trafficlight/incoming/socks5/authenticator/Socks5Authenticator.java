package org.moelab.trafficlight.incoming.socks5.authenticator;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.socksx.v5.Socks5AuthMethod;

/**
 * @author xp
 */
public interface Socks5Authenticator {

    Socks5AuthMethod authMethod();

    void inject(ChannelHandlerContext ctx);
}
