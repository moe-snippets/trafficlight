package org.moelab.trafficlight.incoming.socks5.authenticator;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.socksx.v5.Socks5AuthMethod;

/**
 * @author xp
 */
public class Socks5NopAuthenticator implements Socks5Authenticator {

    @Override
    public Socks5AuthMethod authMethod() {
        return Socks5AuthMethod.NO_AUTH;
    }

    @Override
    public void inject(ChannelHandlerContext ctx) {
    }
}
