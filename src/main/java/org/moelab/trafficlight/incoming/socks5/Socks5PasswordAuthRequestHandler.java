package org.moelab.trafficlight.incoming.socks5;

import org.moelab.trafficlight.incoming.socks5.authenticator.Socks5PasswordAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.*;
import io.netty.handler.codec.socksx.v5.*;

@ChannelHandler.Sharable
public class Socks5PasswordAuthRequestHandler extends SimpleChannelInboundHandler<DefaultSocks5PasswordAuthRequest> {

    private static final Logger LOG = LoggerFactory.getLogger(Socks5PasswordAuthRequestHandler.class);

    private final Socks5PasswordAuthenticator authenticator;

    public Socks5PasswordAuthRequestHandler(Socks5PasswordAuthenticator authenticator) {
        this.authenticator = authenticator;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DefaultSocks5PasswordAuthRequest msg) throws Exception {
        ChannelPipeline pipeline = ctx.pipeline();
        pipeline.remove(Socks5PasswordAuthRequestDecoder.class.getName());
        pipeline.remove(this);

        if (authenticator.authenticate(msg.username(), msg.password())) {
            accepted(ctx, msg);
        } else {
            rejected(ctx, msg);
        }
    }

    private void accepted(ChannelHandlerContext ctx, DefaultSocks5PasswordAuthRequest msg) {
        Socks5PasswordAuthResponse response = new DefaultSocks5PasswordAuthResponse(Socks5PasswordAuthStatus.SUCCESS);
        ctx.writeAndFlush(response);
    }

    private void rejected(ChannelHandlerContext ctx, DefaultSocks5PasswordAuthRequest msg) {
        Socks5PasswordAuthResponse response = new DefaultSocks5PasswordAuthResponse(Socks5PasswordAuthStatus.FAILURE);
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }
}
