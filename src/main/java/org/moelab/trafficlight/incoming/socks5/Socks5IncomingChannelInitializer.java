package org.moelab.trafficlight.incoming.socks5;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.socksx.v5.Socks5CommandRequestDecoder;
import io.netty.handler.codec.socksx.v5.Socks5InitialRequestDecoder;
import io.netty.handler.codec.socksx.v5.Socks5ServerEncoder;

/**
 * @author xp
 */
public class Socks5IncomingChannelInitializer extends ChannelInitializer<SocketChannel> {

    private final Socks5IncomingConfig config;

    public Socks5IncomingChannelInitializer(Socks5IncomingConfig config) {
        this.config = config;
    }

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();

        // convert Socks5Message response to ByteBuf
        pipeline.addLast(
                Socks5ServerEncoder.class.getName(),
                Socks5ServerEncoder.DEFAULT);

        pipeline.addLast(
                Socks5InitialRequestDecoder.class.getName(),
                new Socks5InitialRequestDecoder());
        pipeline.addLast(
                Socks5InitialRequestHandler.class.getName(),
                new Socks5InitialRequestHandler(config));

        pipeline.addLast(
                Socks5CommandRequestDecoder.class.getName(),
                new Socks5CommandRequestDecoder());
        pipeline.addLast(
                Socks5CommandRequestHandler.class.getName(),
                new Socks5CommandRequestHandler(config));
    }
}
