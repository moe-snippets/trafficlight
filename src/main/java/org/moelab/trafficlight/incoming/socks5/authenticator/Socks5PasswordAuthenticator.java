package org.moelab.trafficlight.incoming.socks5.authenticator;

import org.moelab.trafficlight.incoming.socks5.Socks5PasswordAuthRequestHandler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.socksx.v5.Socks5AuthMethod;
import io.netty.handler.codec.socksx.v5.Socks5PasswordAuthRequestDecoder;

/**
 * @author xp
 */
public class Socks5PasswordAuthenticator implements Socks5Authenticator {

    private final Socks5PasswordAuthRequestHandler handler = new Socks5PasswordAuthRequestHandler(this);

    @Override
    public Socks5AuthMethod authMethod() {
        return Socks5AuthMethod.PASSWORD;
    }

    @Override
    public void inject(ChannelHandlerContext ctx) {
        ChannelPipeline pipeline = ctx.pipeline();
        pipeline.addFirst(
                Socks5PasswordAuthRequestDecoder.class.getName(),
                new Socks5PasswordAuthRequestDecoder());
        pipeline.addAfter(
                Socks5PasswordAuthRequestDecoder.class.getName(),
                Socks5PasswordAuthRequestHandler.class.getName(),
                handler);
    }

    public boolean authenticate(String username, String password) {
        return true;
    }
}
