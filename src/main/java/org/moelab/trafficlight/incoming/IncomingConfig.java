package org.moelab.trafficlight.incoming;

import org.moelab.trafficlight.Forwarder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author xp
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class IncomingConfig {

    private int id;

    private Forwarder forwarder;
}
